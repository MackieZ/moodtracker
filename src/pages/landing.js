import LANDING_PAGE_1 from '../assets/landing_page_1.svg'
import LANDING_PAGE_2 from '../assets/landing_page_2.svg'
import LANDING_PAGE_3 from '../assets/landing_page_3.svg'
import LANDING_PAGE_4 from '../assets/landing_page_4.svg'
import React from 'react'
import styled from 'styled-components'
import { pathRoutes } from '../routes'

const Wrapper = styled.div``

const Button = styled.div`
  padding: 1em;
  background: black;
  color: white;
  width: 100%;
  margin: 1.5em 1em;
  margin-left: 0;
  margin-right: 0;
  box-sizing: border-box;
`

const Landing = ({ history }) => {
  const navToLogin = () => {
    history.push(pathRoutes.Login.path)
  }

  return (
    <Wrapper>
      <img src={LANDING_PAGE_1} alt={'landing-page'} />
      <img src={LANDING_PAGE_2} alt={'landing-page'} />
      <img src={LANDING_PAGE_3} alt={'landing-page'} />
      <img src={LANDING_PAGE_4} alt={'landing-page'} />
      <Button onClick={navToLogin}>Get started</Button>
    </Wrapper>
  )
}

export default Landing

// const Home = ({ history }) => {
//
//   return (
//     <Layout>
//       <Navbar> Home </Navbar>
//       <ContentWrapper>{moods}</ContentWrapper>
//     </Layout>
//   )
// }
//
// export default Home
